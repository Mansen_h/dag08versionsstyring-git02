package testPack;

public class Opgave1 {

	public static int fac(int n) {
		int result = 0;
		//termineringregel
		if(n == 0) {
			return 1;
			//rekurensregel, den del af formlen som ændrer i formlen
		} else if(n > 0){
			result = n*fac(n-1);
			
			
		}
		return result;
	}
	
	
	public static void main(String[] args) {
		System.out.println(fac(4));

	}

}
